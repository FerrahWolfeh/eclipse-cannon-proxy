use std::time::Instant;

use log::debug;
use memchr::memmem;

pub const INIT_RP: &[u8] = b"HTTP/1.1 101 Eclipse Cannon, FIRE!\r\n\r\n";

pub enum Source {
    Server,
    Client,
}

#[inline(always)]
pub fn check_first_packet(input: &[u8], always_discard: bool) -> bool {
    let start = Instant::now();
    let scan = memmem::find(input, b"X-Eclipse-Cannon");
    debug!("Header scan took {:?}", start.elapsed());

    if scan.is_some() || always_discard {
        debug!("Dropping first packet.");
        return false;
    }

    debug!("Received generic proxy packet.");
    debug!("Using passthrough mode");

    true
}
