use std::io::{Error, Read, Write};
use std::net::{Shutdown, TcpListener};
use std::panic;
use std::process::exit;
use std::sync::atomic::{AtomicU64, Ordering};
use std::thread::{sleep, spawn};
use std::time::{Duration, Instant};
use std::{
    net::{SocketAddr, TcpStream},
    sync::Arc,
};

use bytesize::ByteSize;
use chrono::Local;
use log::{debug, error, info, trace};
use owo_colors::OwoColorize;

use crate::utils::{check_first_packet, Source, INIT_RP};

pub fn standard_path(internal_addr: SocketAddr, external_addr: SocketAddr, discard_only: bool) {
    let num_conns: Arc<AtomicU64> = Default::default();

    spawn({
        let num_conns = num_conns.clone();

        move || loop {
            sleep(Duration::from_secs(10));
            info!(
                "{} [{} active connections | {}]",
                "STATUS REPORT".green().bold(),
                num_conns.load(Ordering::SeqCst),
                Local::now().format("%H:%M:%S - %d/%M/%Y")
            );
        }
    });

    spawn({
        let num_conns = num_conns.clone();

        move || {
            let mut is_waiting = false;
            loop {
                if num_conns.load(Ordering::SeqCst) > 0 {
                    is_waiting = false;
                } else if !is_waiting {
                    info!("No active connections left.");
                    info!("Waiting for connections...");
                    is_waiting = true;
                }
                sleep(Duration::from_secs(1));
            }
        }
    });

    let listener = TcpListener::bind(external_addr).unwrap_or_else(|_| {
        error!("Address {external_addr} already in use");
        exit(1)
    });

    while let Ok((ingress, client_addr)) = listener.accept() {
        let num_conns = num_conns.clone();
        spawn(move || {
            let mut prx = Proxy::new(ingress, internal_addr, client_addr);

            num_conns.fetch_add(1, Ordering::SeqCst);
            if let Err(error) = prx.proxy_conn(discard_only) {
                error!("Failed to start proxy thread. Error: {}", error)
            }
            num_conns.fetch_sub(1, Ordering::SeqCst);
        });
    }
}

struct Proxy {
    target_addr: SocketAddr,
    client_addr: SocketAddr,
    client_stream: Arc<TcpStream>,
}

impl Proxy {
    pub fn new(client_stream: TcpStream, t_addr: SocketAddr, client_addr: SocketAddr) -> Self {
        Self {
            target_addr: t_addr,
            client_addr,
            client_stream: Arc::new(client_stream),
        }
    }

    fn proxy_conn(&mut self, discard_only: bool) -> Result<(), Error> {
        panic::set_hook(Box::new(|msg| {
            println!("{}", msg);
        }));

        info!("Accepted connection from {}", self.client_addr.green());

        let tx_counter = Arc::new(AtomicU64::default());
        let rx_counter = Arc::new(AtomicU64::default());

        let start = Instant::now();

        let tgt_stream = match TcpStream::connect_timeout(&self.target_addr, Duration::from_secs(2))
        {
            Ok(s) => s,
            Err(error) => {
                panic!("Failed to init target connection. Error: {}", error);
            }
        };

        let tgt_arc = Arc::new(tgt_stream);

        let (mut client_tx, mut client_rx) = (
            self.client_stream.try_clone()?,
            self.client_stream.try_clone()?,
        );

        let (mut target_tx, mut target_rx) = (tgt_arc.try_clone()?, tgt_arc.try_clone()?);

        let (tx, rx) = (tx_counter.clone(), rx_counter.clone());

        let mut discard_buf = [0u8; 1024];

        let buffer_size = client_rx.read(&mut discard_buf)?;
        debug!("Filled discard buffer with {buffer_size} bytes");

        if check_first_packet(&discard_buf, discard_only) {
            let _ = target_tx.write_all(&discard_buf[..buffer_size]);
        }

        client_tx.write_all(INIT_RP)?;
        client_tx.flush()?;

        let threads = vec![
            spawn(move || Self::standard_copy(&mut client_tx, &mut target_rx, Source::Client, rx)),
            spawn(move || Self::standard_copy(&mut target_tx, &mut client_rx, Source::Server, tx)),
        ];

        for t in threads {
            match t.join() {
                Ok(_) => {}
                Err(_) => {
                    error!("Connection error");
                }
            }
        }

        info!("Closing connection for {}", self.client_addr.green());
        debug!(
            "{} | time: {:.2?}, tx {}, rx {}",
            self.client_addr,
            start.elapsed(),
            ByteSize::b(tx_counter.load(Ordering::SeqCst)).to_string_as(true),
            ByteSize::b(rx_counter.load(Ordering::SeqCst)).to_string_as(true)
        );
        self.client_stream.shutdown(Shutdown::Both)?;

        Ok(())
    }

    #[inline]
    fn standard_copy(
        from: &mut TcpStream,
        to: &mut TcpStream,
        source: Source,
        counter: Arc<AtomicU64>,
    ) -> Result<(), std::io::Error> {
        let mut buf = [0u8; 1024 * 32];
        loop {
            // The standard procedure for a proxy using std can be just a simple direct
            // copy to the sender socket without intermediate buffers,
            // but since we might want to see what is inside the packets received,
            // this function preallocates a small one.
            let res = from.read(&mut buf);
            let start = Instant::now();

            // Propagate errors, see how many bytes we read
            let n = res?;
            if n == 0 {
                // A read of size zero signals EOF (end of file), finish gracefully
                return Ok(());
            }

            // In case something in the transport doesn't quite work, we can analyze what's being transferred.
            match source {
                Source::Client => {
                    trace!("Client Response: {:?}", String::from_utf8_lossy(&buf[..n]))
                }
                Source::Server => {
                    trace!("Server Response: {:?}", String::from_utf8_lossy(&buf[..n]))
                }
            }

            // Now we "send" all the data we received to the target.
            let res = to.write(&buf[..n]);
            let end = start.elapsed();
            trace!(
                "Transfer speed of: {}/s",
                ByteSize::b((n as f32 / end.as_secs_f32()).round() as u64)
            );

            counter.fetch_add(res? as u64, Ordering::SeqCst);
        }
    }
}
