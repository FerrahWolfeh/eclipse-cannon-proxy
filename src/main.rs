use clap::Parser;
use clap::ValueEnum;
use env_logger::Builder as Logger;
use log::info;
use log::warn;
use owo_colors::OwoColorize;
use std::net::SocketAddr;
use std::process::ExitCode;

mod standard;
mod utils;

use crate::standard::standard_path;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// External, Network-facing port
    #[clap(short, long, default_value_t = 80)]
    ext_port: u16,

    /// Internal, sshd-facing port
    #[clap(short, long, default_value_t = 22)]
    in_port: u16,

    /// Enables listening on 0.0.0.0
    #[clap(long, default_value_t = false)]
    listen: bool,

    /// Always discard first packet (stops working as a regular proxy)
    #[clap(long, default_value_t = false)]
    discard_only: bool,
}

#[derive(Clone, Copy, ValueEnum, Debug)]
enum Mode {
    Standard,
    Uring,
}

fn main() -> ExitCode {
    let args = Args::parse();
    Logger::from_default_env().format_timestamp(None).init();

    let external_addr = if args.listen {
        SocketAddr::from(([0, 0, 0, 0], args.ext_port))
    } else {
        SocketAddr::from(([127, 0, 0, 1], args.ext_port))
    };
    let internal_addr = SocketAddr::from(([127, 0, 0, 1], args.in_port));

    info!(
        "Proxying TCP packets from {} -> {}",
        external_addr.bold().bright_blue(),
        internal_addr.bold().bright_yellow()
    );

    if args.discard_only {
        warn!("Discard-only flag is set! Normal proxy functionality may not work");
    }

    standard_path(internal_addr, external_addr, args.discard_only);
    ExitCode::SUCCESS
}
